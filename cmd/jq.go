/*
Copyright © 2020 Arthur Dayne <palagend@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"io"
	"os"

	"github.com/savaki/jq"
	"github.com/spf13/cobra"
)

// jqCmd represents the jq command
var jqCmd = &cobra.Command{
	Use:   "jq",
	Short: "A high performance Golang implementation of jq command line tool",
	Long: `jq is a lightweight and flexible command-line JSON processor.

If you want to learn to use jq, read the documentation at https://stedolan.github.io/jq. You can also try it online at jqplay.org.`,
	Run: func(cmd *cobra.Command, args []string) {
		filter := "."
		if len(args) > 0 {
			filter = args[0]
		}
		data := []byte{}
		if isInputFromPipe() {
			p := make([]byte, 1024)
			for {
				n, err := os.Stdin.Read(p)
				if err != nil && err != io.EOF {
					panic("Read error: " + err.Error())
				} else if n == 0 {
					break
				} else {
					data = append(data, p[:n]...)
				}
			}
		} else if len(args) > 1 {
			data = []byte(args[1])
		} else {
			panic("Invalid input")
		}
		runCommand(filter, data)
	},
}

func init() {
	rootCmd.AddCommand(jqCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// jqCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// jqCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func runCommand(filter string, data []byte) {
	op, _ := jq.Parse(filter)
	result, _ := op.Apply([]byte(data))
	os.Stdout.Write(result)
}

func isInputFromPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
