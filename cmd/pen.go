/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// penCmd represents the pen command
var penCmd = &cobra.Command{
	Use:   "pen",
	Short: "Generate various fonts of cursive script.",
	Run: func(cmd *cobra.Command, args []string) {
		font, err := cmd.Flags().GetInt("font")
		if err != nil {
			logrus.Error(err)
		}
		doWrite(args, font)
	},
}

func doWrite(args []string, font int) {
	var content string
	if len(args) < 1 {
		content = "NoWord"
	} else {
		content = args[0]
	}
	baseURL := "http://www.diyiziti.com/"
	v := url.Values{}
	fontInfoID := strconv.Itoa(font)
	v.Set("FontInfoId", fontInfoID)
	v.Set("FontColor", "#000000")
	v.Set("FontSize", "42")
	v.Set("ImageWidth", "280")
	v.Set("ImageHeight", "100")
	v.Set("ActionCategory", "2")
	v.Set("Content", content)
	vStr := v.Encode()
	logrus.Info(vStr)

	//利用指定的method,url以及可选的body返回一个新的请求.
	//如果body参数实现了io.Closer接口，Request返回值的Body 字段会被设置为body，并会被Client类型的Do、Post和PostFOrm方法以及Transport.RoundTrip方法关闭。
	data := ioutil.NopCloser(strings.NewReader(vStr)) //把form数据编下码
	client := &http.Client{}                          //客户端,被Get,Head以及Post使用
	reqest, err := http.NewRequest("POST", baseURL, data)
	if err != nil {
		logrus.Error(err)
	}
	//给一个key设定为响应的value.
	reqest.Header.Set("Content-Type", "application/x-www-form-urlencoded") //必须设定该参数,POST参数才能正常提交

	res, err := client.Do(reqest) //发送请求

	if err != nil {
		logrus.Error(err)
	}

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)
	filename := os.Getenv("HOME") + "/" + content + ".png"
	ioutil.WriteFile(filename, body, 0640)
}

func init() {
	rootCmd.AddCommand(penCmd)

	penCmd.Flags().IntP("font", "f", 493, `Designate the font code， the code list is:
	391, 426, 90, 443, 432, 93, 434, 104, 445, 99, 268, 450, 341, 167, 513, 433,
	498, 87, 405, 390, 470, 455, 207, 83, 507, 465, 413, 351, 151, 478, 472, 294,
	511, 528, 531, 532, 522, 157, 505, 497, 493, 490, 488, 481, 483, 480, 509, 62,
	477, 475, 476, 474, 473, 471, 469, 467, 466, 462, 453, 452, 440, 436, 103, 105,
	136, 321, 95, 82, 113, 515, 114, 117, 92, 58, 69, 35, 81, 73, 118, 36, 219, 150,
	148, 189, 175, 140, 155, 200, 85, 86, 108, 128, 141, 146, 139, 134, 88, 192, 152,
	241, 227, 234, 273, 270, 491, 482, 383, 163, 205, 91, 64, 60, 61, 55, 56, 57, 106,
	123, 124, 265, 125, 138, 40, 53, 54, 38, 65, 94, 66, 75, 63, 160, 122, 120, 203, 418,
	502, 503, 504, 506, 499, 500, 501, 510, 508, 512, 514, 489, 484, 485, 486, 494, 495,
	496, 441, 442, 451, 446, 447, 448, 449, 454, 463, 464, 456, 457, 458, 459, 460, 461,
	468, 479, 516, 517, 518, 519, 520, 521, 523, 524, 525, 527, 529, 530, 420, 421, 422,
	423, 424, 425, 427, 428, 429, 430, 431, 437, 438, 439, 435, 271, 272, 414, 415, 416,
	417, 406, 407, 408, 409, 410, 411, 412, 266, 267, 269, 384, 385, 386, 387, 389, 392,
	393, 394, 395, 396, 397, 398, 399, 401, 402, 404, 352, 353, 354, 355, 356, 357, 358,
	359, 361, 362, 364, 365, 366, 367, 368, 369, 370, 372, 373, 374, 375, 376, 377, 378,
	380, 381, 382, 274, 275, 277, 278, 279, 280, 281, 283, 284, 285, 286, 287, 288, 289, 
	290, 291, 292, 293, 295, 296, 297, 298, 299, 300, 301, 303, 304, 305, 306, 307, 308,
	309, 311, 312, 313, 314, 315, 316, 317, 318, 319, 235, 236, 237, 238, 239, 240, 228,
	229, 230, 231, 342, 343, 344, 345, 346, 347, 348, 350, 322, 323, 324, 325, 326, 327,
	328, 329, 330, 331, 333, 334, 336, 339, 340, 204, 206, 220, 221, 222, 225, 226, 208,
	209, 210, 211, 212, 213, 214, 215, 217, 218, 242, 243, 245, 246, 247, 248, 249, 250,
	251, 252, 254, 255, 256, 257, 258, 259, 261, 263, 264, 161, 162, 158, 159, 164, 165,
	170, 153, 154, 156, 195, 196, 198, 199, 201, 202, 190, 191, 176, 179, 180, 182, 
	183, 185, 186, 187, 121, 119, 126, 109, 115, 116, 135, 129, 130, 131, 137, 144, 145,
	39, 37, 100, 84`)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// penCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// penCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
